package sample;

import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import java.io.File;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ImageView i1;
    public ImageView i2;
    public ImageView i3;
    public ImageView i4;
    public ImageView i5;
    public ImageView i6;
    public ImageView i7;
    public ImageView i8;
    public ImageView i9;
    int step , Previous ,room;
    String[] AllFile = {"1.jpg","2.jpg","3.jpg","4.jpg","5.jpg"
            ,"6.jpg","7.jpg","8.jpg","9.jpg"};
    //int[] GetFile = {0,1,2,3,4,5,6,7,8};
    int[] GetFile = new int[9];

    //剛開始預設
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        step = 0;
        Previous = 0 ;

        doRandom();
        doSetUpImage();
    }

    public void doCI1(MouseEvent mouseEvent) {
        doExchangeLocation(1);
    }

    public void doCI2(MouseEvent mouseEvent) {
        doExchangeLocation(2);
    }

    public void doCI3(MouseEvent mouseEvent) {
        doExchangeLocation(3);
    }

    public void doCI4(MouseEvent mouseEvent) {
        doExchangeLocation(4);
    }

    public void doCI5(MouseEvent mouseEvent) {
        doExchangeLocation(5);
    }

    public void doCI6(MouseEvent mouseEvent) {
        doExchangeLocation(6);
    }

    public void doCI7(MouseEvent mouseEvent) {
        doExchangeLocation(7);
    }

    public void doCI8(MouseEvent mouseEvent) {
        doExchangeLocation(8);
    }

    public void doCI9(MouseEvent mouseEvent) {
        doExchangeLocation(9);
    }

    //步驟 "0" or "1"
    public void doExchangeLocation(int In){
        System.out.println(In+"位置");
        if(step == 0){
            Previous = In;
            step++;
        }else {
            doExchangeImage(In);
            step = 0;
        }

    }

    //在這裡交換圖片和判斷可否交換
    public void doExchangeImage( int In ){
        if( In == Previous ){
            System.out.println("一樣的交換沒意義");
        }else {
            if( (Previous == 1 && ( In == 4 || In == 2 )) || (Previous == 3 && ( In == 6 || In == 2 )) ||
                    (Previous == 7 && ( In == 4 || In == 8 )) || (Previous == 9 && ( In == 6 || In == 8 )) ||
                    (Previous == 2 && ( In == 1 || In == 3 || In == 5)) ||
                    (Previous == 4 && ( In == 1 || In == 5 || In == 7)) ||
                    (Previous == 6 && ( In == 3 || In == 5 || In == 9)) ||
                    (Previous == 8 && ( In == 7 || In == 5 || In == 9)) ||
                    (Previous == 5 && ( In == 2 || In == 4 || In == 6 || In == 8)) ) {
                System.out.println(In + "和" + Previous + "交換");
                room = GetFile[Previous - 1];
                GetFile[Previous - 1] = GetFile[In - 1];
                GetFile[In - 1] = room;
                doSetUpImage();
                System.out.println("優質");
                isWin();
            }else {
                System.out.println("這不是可以交換的地方");
            }
        }
    }

    //改變圖片
    public void doSetUpImage(){
        File file=new File(AllFile[GetFile[0]]);
        File file1=new File(AllFile[GetFile[1]]);
        File file2=new File(AllFile[GetFile[2]]);
        File file3=new File(AllFile[GetFile[3]]);
        File file4=new File(AllFile[GetFile[4]]);
        File file5=new File(AllFile[GetFile[5]]);
        File file6=new File(AllFile[GetFile[6]]);
        File file7=new File(AllFile[GetFile[7]]);
        File file8=new File(AllFile[GetFile[8]]);
        i1.setImage(new Image(file.toURI().toString()));
        i2.setImage(new Image(file1.toURI().toString()));
        i3.setImage(new Image(file2.toURI().toString()));
        i4.setImage(new Image(file3.toURI().toString()));
        i5.setImage(new Image(file4.toURI().toString()));
        i6.setImage(new Image(file5.toURI().toString()));
        i7.setImage(new Image(file6.toURI().toString()));
        i8.setImage(new Image(file7.toURI().toString()));
        i9.setImage(new Image(file8.toURI().toString()));
    }

    //把數值打亂
    public void doRandom(){
        for( int i  = 0 ; i< GetFile.length ; i++ ){
            GetFile[i] = -1;
        }
        Random random = new Random();
        for(int i = 0 ; i < GetFile.length ;i++){

            GetFile[i] = (int) (Math.random()*9);
            System.out.println("|"+GetFile[i]+"|");

            for(int j = 0 ; j < i ;j++){
                if(GetFile[j] == GetFile[i]){
                    i--;
                    break;
                }

            }


        }
    }

    //確認是否完成遊戲
    public void isWin(){
        for(int i = 0 ; i< 9 ; i++ ){
            if(GetFile[i] != i ){
                System.out.println("還沒完成");
                return;
            }
        }
        doWin();
        System.out.println("恭喜完成");
    }

    //遊戲完成
    public void doWin(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("提示");
        alert.setHeaderText(null);
        alert.setContentText("遊戲完成～～～～");

        alert.showAndWait();
    }
}
